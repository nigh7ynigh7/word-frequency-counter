const express = require('express')

const app = express();

const { Readable } = require('stream');
const cp = require('child_process')

var executor = (args='', stdin='') => new Promise((yes, no)=>{
    let child = cp.exec('py word_frequency.py ' + args + (stdin ? ' -stdin' : ''), function (err, stdout, stderr){
        if(stderr || err) {
            return no(stderr || err);
        }
        if(!stdout){
            yes({})
        }
        else{
            let obj = JSON.parse(stdout)
            yes(obj);
        }
    });
    if(stdin) {
        // child.stdin.write(stdin)
        // child.stdin.end()
        let toSend = new Readable();
        toSend.push(stdin);
        toSend.push('\0')
        toSend.push(null);
        toSend.pipe(child.stdin);
    }
});

app.use((req,res,next)=>{
    console.log(
        new Date().toTimeString(),
        req.method,
        req.path);
    next()
});

app.use(function(req, res, next){
    if (req.is('text/*')) {
      req.text = '';
      req.setEncoding('utf8');
      req.on('data', function(chunk){ req.text += chunk });
      req.on('end', next);
    } else {
      next();
    }
});

app.post('/frequency', (req,res,next)=>{
    if(!req.text) return res.json({});
    executor('count' in req.query ? '-count' : '', req.text)
    .then((x)=>{
        res.json(x)
    })
    .catch((err)=>{
        res.status(500).json({ data : err })
    })
})

app.get('*', (req,res,next)=>{
    res.json({ title : 'Welcome', subTitle : 'Welcome to the service' })
})

app.listen(3000, ()=>{
    console.log('Running at http://localhost:3000/');
});