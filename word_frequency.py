
import os, re, collections;

FILENAME = os.path.join(os.environ.get('FILENAME') or 'input.txt')

if not os.path.exists(FILENAME):
    print('File {} does not exist'.format(FILENAME))
    exit(1)

def get_regexp():
    output = r'[^a-z]'
    with open('regexp.txt', 'r', encoding='utf-8') as fa:
        output = fa.readline()
    return output;

def add_or_init(dic={}, word=''):
    to_set = dic.setdefault(word, 0) + 1
    dic[word] = to_set

def sort_dict_by_key(words={}, inverse=False):
    temp_dict={}
    for key, value in words.items():
        val = temp_dict.setdefault(value, [])
        val.append(key)
    sort = sorted(temp_dict);
    if(inverse):
        sort.reverse()
    output = {}
    for key in sort:
        for values in temp_dict[key]:
            output[values] = key
    return output

def print_dict(dic):
    first = True
    if dic:
        print('{')
        for key, val in dic.items():
            if not first:
                print(',')
            print('  "{}" : {}'.format(key, val), end="")
            first = False
        print('\n}')


IGNORE=re.compile(get_regexp())
words = {}
frequency_map = {}

import sys

is_stream = '-stdin' in sys.argv

with (sys.stdin if is_stream else open(FILENAME, encoding="utf-8" )) as file:
    
    word = ''
    total = 0
    if is_stream and sys.stdin.isatty():
        exit(1)

    while file.readable():
        byte = file.read(1)
        if not byte:
            break
        if IGNORE.match(byte) and word:
            add_or_init(words, word.lower())
            word = ''
            total += 1
        elif not IGNORE.match(byte):
            word += byte
    for key in words.keys():
        frequency_map.setdefault(key, words[key] / total)



if '-count' in sys.argv:
    print_dict(sort_dict_by_key(words, True))
else:
    print_dict(sort_dict_by_key(frequency_map, True))