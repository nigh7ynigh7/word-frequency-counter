# Word frequency Counter

Requires python and node

The `python` part:

    python3 word_frequency.py

The `node` part:

    node reader.js

## What's going on?

`Python` does the calculation shenanigans.

`Node` calls the python stuff.